from typing import List, Tuple, Dict, Set

Liste1:List[Tuple[str,int]]=[('Riri', 75),('Fifi', 54),('Loulou', 87)]

def taille(classe:List[Tuple[str,int]])->List[Tuple[str,int]]:
    liste_triée:List[Tuple[str,int]]=[]
    for x in range(len(classe)):
        taille_max:int=0
        for couple in classe:
            if couple[1]>taille_max:
                couple_max:Tuple[str,int]=couple
                taille_max=couple[1]
        del classe[classe.index(couple_max)]
        liste_triée.append(couple_max)
    return(liste_triée)

print(taille(Liste1))